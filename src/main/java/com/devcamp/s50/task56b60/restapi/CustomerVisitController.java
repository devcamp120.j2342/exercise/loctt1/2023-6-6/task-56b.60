package com.devcamp.s50.task56b60.restapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerVisitController {
     @GetMapping("/visits")
     public ArrayList<Visit> visitsList() {
          Customer customer1 = new Customer("Truong Tan Loc", false, "Premium");
          Customer customer2 = new Customer("Vuong My Cam", true, "Premium");
          Customer customer3 = new Customer("Truong Phat Tai", false, "Basic");
          System.out.println(customer1.toString());
          System.out.println(customer2.toString());
          System.out.println(customer3.toString());
  
          Visit visit1 = new Visit(customer3, new Date(), 1200.0, 10);
          Visit visit2 = new Visit(customer2, new Date(), 1300.0, 20);
          Visit visit3 = new Visit(customer1, new Date(), 1400.0, 30);
          System.out.println(visit1.toString());
          System.out.println(visit2.toString());
          System.out.println(visit3.toString());
          ArrayList<Visit> visits = new ArrayList<>();
          visits.add(visit3);
          visits.add(visit2);
          visits.add(visit1);
          return visits;
  
}
}
